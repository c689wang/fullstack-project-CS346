package store

data class SetKtorToken(val token: String)

data class SetSpringToken(val token: String)

data class SetUserID(val userId: String)

data class SetCalendarTheme(val calendarTheme: String)

class LogoutUser
