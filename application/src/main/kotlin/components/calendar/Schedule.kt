package components.calendar

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.PlainTooltipBox
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.ParentDataModifier
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalWindowInfo
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import models.UserCourse
import java.security.KeyStore.TrustedCertificateEntry
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import kotlin.math.roundToInt
import java.text.SimpleDateFormat
import java.util.*
import java.time.DayOfWeek
import java.time.temporal.TemporalAdjusters
import compose.icons.TablerIcons
import compose.icons.tablericons.CaretRight
import compose.icons.tablericons.CaretLeft

data class UniClass(

    // STAT333
    val name :String,

    // LEC, TUT, TST
    val type :String,

    // color
    val color :Color,

    val days: String,

    // start time
    // "1:30:00"
    val start :LocalDateTime,

    // finish time
    val finish :LocalDateTime,

    val startDate: LocalDateTime,

    val endDate: LocalDateTime
)

enum class Theme {
    OCEAN, PASTEL, SUNSET, EARTH
}

val TimeFormatter = DateTimeFormatter.ofPattern("h:mma")
val HourFormatter = DateTimeFormatter.ofPattern("h a")
val DateFormatter = DateTimeFormatter.ofPattern("MMM dd")

// allows us to attach data to a composable with a modifier
// read data from a measurable within a layout
// we need to do this because we need to know the underlying
private class ClassDataModifier(
    val uniclass: UniClass,
) : ParentDataModifier {
    override fun Density.modifyParentData(parentData: Any?) = uniclass
}

// add customer modifier to attach data as parentData to composable
private fun Modifier.classData(uniclass: UniClass) = this.then(ClassDataModifier(uniclass))

// Global variables for the size of the calendar
val startTime = LocalTime.parse("08:00:00")
val endTime = LocalTime.parse("22:00:00")
val hours = ChronoUnit.HOURS.between(startTime, endTime).toInt()



@OptIn(ExperimentalComposeUiApi::class)
@Composable
@Preview
fun oneClass (
    uniclass: UniClass,
    textSize: Int,
    modifier: Modifier = Modifier
) {
    Column (
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 1.dp, end = 1.dp)
            .background(uniclass.color, shape = RoundedCornerShape(4.dp)),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {

        val density = LocalWindowInfo.current.containerSize.width / 3 / 7
        SelectionContainer {
            Text(uniclass.name + " " + uniclass.type,

                fontSize = textSize.sp)
        }
        SelectionContainer {
            Text(uniclass.start.format(TimeFormatter).replace(".", "").uppercase() + " - " + uniclass.finish.format(TimeFormatter).replace(".", "").uppercase(),
                fontSize = textSize.sp)
        }
    }
}

// SOURCE: THE SIDEBAR AND THE SCHEDULE FUNCTION were obtained from source
// https://medium.com/@meytataliti/android-simple-calendar-with-jetpack-compose-662e4d1794b
// gave us an initial start to the project, and learned about custom layouts.

@Composable
fun BasicSidebarLabel(
    time: LocalTime,
    modifier: Modifier = Modifier,
) {
    Text(
        text = time.format(HourFormatter).replace(".", "").uppercase(),
        fontSize = 12.sp,
        modifier = modifier
            .fillMaxHeight()
            .padding(start = 2.dp)
    )
}

@Composable
fun ScheduleSidebar(
    hoursHeigh: Dp,
    modifier: Modifier = Modifier,
    label: @Composable (time: LocalTime) -> Unit = { BasicSidebarLabel(time = it) },
) {
    Column(modifier = modifier) {
        repeat(hours) { i ->
            Box(modifier = Modifier.height(hoursHeigh)) {
                label(startTime.plusHours(i.toLong()))
            }
        }
    }
}


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Schedule(
    hourHeight: Dp,
    classes: List<UniClass>,
    textSize: Int,
    modifier: Modifier = Modifier,
    uniclassContent: @Composable (uniclass: UniClass) -> Unit = { oneClass(uniclass = it, textSize) },
) {

    Layout(
        content = {
            classes.sortedBy(UniClass::start).forEach { uniclass ->
                // attach the class data to the composable
                Box(modifier = Modifier.classData(uniclass)) {
                    uniclassContent(uniclass)
                }
            }
        },
        modifier = modifier
            //.verticalScroll(rememberScrollState()),

    ) { classMeasureables, constraints ->

        val height = hourHeight.roundToPx() * hours
        val placeablesWithClasses = classMeasureables.map { measurable ->
            val uniclass = measurable.parentData as UniClass
            val classDurationMinutes = ChronoUnit.MINUTES.between(uniclass.start, uniclass.finish)
            val classHeight = ((classDurationMinutes / 60f) * hourHeight.toPx()).roundToInt()
            val placeable = measurable.measure(constraints.copy(minHeight = classHeight, maxHeight = classHeight))
            Pair(placeable, uniclass)
        }

        layout(constraints.maxWidth, height) {
            placeablesWithClasses.forEach { (placeable, uniclass) ->
                val eventOffsetMinutes = ChronoUnit.MINUTES.between(LocalTime.parse("08:00:00"), uniclass.start.toLocalTime())
                val eventY = ((eventOffsetMinutes / 60f) * hourHeight.toPx()).roundToInt()
                placeable.place(0, eventY)
            }
        }
    }
}

fun generateColors(courseList: List<UserCourse>, theme: Theme): Map<String, Color> {
    val distinctCourseNames = courseList.map { it.courseNum }.distinct()
    val colorMap = mutableMapOf<String, Color>()

    // Define color sets for each theme
    val theme1Colors = listOf(
        Color(174, 214, 241),
        Color(133, 193, 233),
        Color(93, 173, 226),
        Color(52, 152, 219),
        Color(46, 134, 193),
        Color(127, 179, 213),
        Color(84, 153, 199),
        Color(41, 128, 185),
        Color(174, 214, 241),
        Color(133, 193, 233),
        Color(93, 173, 226),
        Color(52, 152, 219),
        Color(46, 134, 193),
        Color(127, 179, 213),
        Color(84, 153, 199),
        Color(41, 128, 185),
        Color(174, 214, 241),
        Color(133, 193, 233),
        Color(93, 173, 226),
        Color(52, 152, 219),
        Color(46, 134, 193),
        Color(127, 179, 213),
        Color(84, 153, 199),
        Color(41, 128, 185),
        Color(174, 214, 241),
        Color(133, 193, 233),
        Color(93, 173, 226),
        Color(52, 152, 219),
        Color(46, 134, 193),
        Color(127, 179, 213),
        Color(84, 153, 199),
        Color(41, 128, 185),
    )

    val theme2Colors = listOf(
        Color(255, 207, 210),
        Color(241, 192, 232),
        Color(207, 186, 240),
        Color(163, 196, 243),
        Color(144, 219, 244),
        Color(142, 236, 245),
        Color(152, 245, 225),
        Color(185, 251, 192),
        Color(251, 248, 204),
        Color(253, 228, 207),
        Color(255, 207, 210),
        Color(241, 192, 232),
        Color(207, 186, 240),
        Color(163, 196, 243),
        Color(144, 219, 244),
        Color(142, 236, 245),
        Color(152, 245, 225),
        Color(185, 251, 192),
        Color(251, 248, 204),
        Color(253, 228, 207),
        Color(207, 186, 240),
        Color(163, 196, 243),
        Color(144, 219, 244),
        Color(142, 236, 245),
        Color(152, 245, 225),
        Color(185, 251, 192),
        Color(251, 248, 204),
        Color(253, 228, 207),
        Color(255, 207, 210),
        Color(241, 192, 232),
        Color(207, 186, 240),
        Color(163, 196, 243),
        Color(144, 219, 244),
        Color(142, 236, 245),
        Color(152, 245, 225),
        Color(185, 251, 192),
        Color(251, 248, 204),
        Color(253, 228, 207),
        )

    val theme3Colors = listOf(
        Color(255,248,182),
        Color(255,189,145),
        Color(255,141,113),
        Color(255,112,126),
        Color(255,213,179),
        Color(255,182,158),
        Color(255,167,145),
        Color(255,248,182),
        Color(255,189,145),
        Color(255,141,113),
        Color(255,112,126),
        Color(255,213,179),
        Color(255,182,158),
        Color(255,167,145),
        Color(255,248,182),
        Color(255,189,145),
        Color(255,141,113),
        Color(255,112,126),
        Color(255,213,179),
        Color(255,182,158),
        Color(255,167,145),
        Color(255,248,182),
        Color(255,189,145),
        Color(255,141,113),
        Color(255,112,126),
        Color(255,213,179),
        Color(255,182,158),
        Color(255,167,145),
        )

    val theme4Colors = listOf(
        Color(170,214,136),
        Color(152,195,119),
        Color(139,189,120),
        Color(94,167,88),
        Color(71,137,75),
        Color(200,225,204),
        Color(184,216,190),
        Color(224,240,227),
        Color(170,214,136),
        Color(152,195,119),
        Color(139,189,120),
        Color(94,167,88),
        Color(71,137,75),
        Color(200,225,204),
        Color(184,216,190),
        Color(224,240,227),
        Color(170,214,136),
        Color(152,195,119),
        Color(139,189,120),
        Color(94,167,88),
        Color(71,137,75),
        Color(200,225,204),
        Color(184,216,190),
        Color(224,240,227),
        Color(170,214,136),
        Color(152,195,119),
        Color(139,189,120),
        Color(94,167,88),
        Color(71,137,75),
        Color(200,225,204),
        Color(184,216,190),
        Color(224,240,227),
    )

    val selectedThemeColors = when (theme) {
        Theme.OCEAN -> theme1Colors
        Theme.PASTEL -> theme2Colors
        Theme.SUNSET -> theme3Colors
        Theme.EARTH -> theme4Colors
    }

    for ((index, courseName) in distinctCourseNames.withIndex()) {
        // Use colors from the selected theme in order
        val colorIndex = index % selectedThemeColors.size
        colorMap[courseName] = selectedThemeColors[colorIndex]
    }

    return colorMap
}



@OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
fun CalendarRender(courseList: List<UserCourse>, selectedTheme: Theme) {

    val colorMap = generateColors(courseList, selectedTheme)

    val selectedCourses =  courseList.map { UniClass(it.courseNum,
        it.component, colorMap[it.courseNum]!!, it.weekPattern, LocalDateTime.parse(it.startTime),
        LocalDateTime.parse(it.endTime), LocalDateTime.parse(it.startDate), LocalDateTime.parse(it.endDate))
    }

    val today = remember { mutableStateOf(LocalDateTime.now()) }
    val dayOfWeek = today.value.dayOfWeek

    val weekDayNumberMap: MutableMap<String, Int> = mutableMapOf()
    weekDayNumberMap["MONDAY"] = 1
    weekDayNumberMap["TUESDAY"] = 2
    weekDayNumberMap["WEDNESDAY"] = 3
    weekDayNumberMap["THURSDAY"] = 4
    weekDayNumberMap["FRIDAY"] = 5
    weekDayNumberMap["SATURDAY"] = 6
    weekDayNumberMap["SUNDAY"] = 7

    val dayOfWeekNumber = weekDayNumberMap[dayOfWeek.toString()]
    val dayOfWeekNumberNonNull = dayOfWeekNumber ?: 0
    val monDate = today.value.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).withHour(0)
        .withMinute(0)
        .withSecond(0)
        .withNano(0)

    val tuesDate = if (2 > dayOfWeekNumberNonNull) {
        today.value.with(TemporalAdjusters.nextOrSame(DayOfWeek.TUESDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    } else {
        today.value.with(TemporalAdjusters.previousOrSame(DayOfWeek.TUESDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    }

    val wedDate = if (3 > dayOfWeekNumberNonNull) {
        today.value.with(TemporalAdjusters.nextOrSame(DayOfWeek.WEDNESDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    } else {
        today.value.with(TemporalAdjusters.previousOrSame(DayOfWeek.WEDNESDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    }

    val thursDate = if (4 > dayOfWeekNumberNonNull) {
        today.value.with(TemporalAdjusters.nextOrSame(DayOfWeek.THURSDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    } else {
        today.value.with(TemporalAdjusters.previousOrSame(DayOfWeek.THURSDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    }

    val friDate = if (5 > dayOfWeekNumberNonNull) {
        today.value.with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    } else {
        today.value.with(TemporalAdjusters.previousOrSame(DayOfWeek.FRIDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    }

    val satDate = if (6 > dayOfWeekNumberNonNull) {
        today.value.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    } else {
        today.value.with(TemporalAdjusters.previousOrSame(DayOfWeek.SATURDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    }

    val sunDate = if (7 > dayOfWeekNumberNonNull) {
        today.value.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    } else {
        today.value.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY)).withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
    }

    val mondayClasses = selectedCourses.filter {(it.startDate <= monDate && monDate <= it.endDate) && (it.days.contains("M"))}

    val tuesdayClasses = selectedCourses.filter {(it.startDate <= tuesDate && tuesDate <= it.endDate) && (it.days.contains("T"))}

    val wednesdayClasses = selectedCourses.filter {(it.startDate <= wedDate && wedDate <= it.endDate) && (it.days.contains("W"))}

    val thursdayClasses = selectedCourses.filter {(it.startDate <= thursDate && thursDate <= it.endDate) && (it.days.contains("R"))}

    val fridayClasses = selectedCourses.filter {(it.startDate <= friDate && friDate <= it.endDate) && (it.days.contains("F"))}

    val saturdayClasses = selectedCourses.filter {(it.startDate <= satDate && satDate <= it.endDate) && (it.days.contains("Sa"))}

    val sundayClasses = selectedCourses.filter {(it.startDate <= sunDate && sunDate <= it.endDate) && (it.days.contains("Su"))}

    val classes = listOf(mondayClasses, tuesdayClasses, wednesdayClasses,
        thursdayClasses, fridayClasses, saturdayClasses, sundayClasses)

    val monDateString = monDate.format(DateFormatter)
    val tuesDateString = tuesDate.format(DateFormatter)
    val wedDateString = wedDate.format(DateFormatter)
    val thursDateString = thursDate.format(DateFormatter)
    val friDateString = friDate.format(DateFormatter)
    val satDateString = satDate.format(DateFormatter)
    val sunDateString = sunDate.format(DateFormatter)

    val days = listOf(("M | " + monDateString),
        ("Tu | " + tuesDateString), ("W | " + wedDateString), ("Th | " + thursDateString),
        ("F | " + friDateString), ("Sa | " + satDateString), ("Su | " + sunDateString))

    var screenHeight = LocalWindowInfo.current.containerSize.height
    var hourHeight = (screenHeight / hours).dp
    if (hourHeight < 40.dp) {
        hourHeight = 40.dp
    }

    //print("hourHeight modified \n")
    //print("hourheight: " + hourHeight + "\n")

    Column (
        modifier = Modifier
            .fillMaxWidth()
    ) {

        // TITLES
        Row (
            modifier = Modifier
                .height(50.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            // TIMES space

            Row (
                modifier = Modifier
                    .width(70.dp)
                    .fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Box (
                    //modifier = Modifier.padding(horizontal = 1.dp, vertical = 1.dp)
                ) {
                    PlainTooltipBox(
                        tooltip = {Text("Next Week", color = Color.White)}
                    ) {
                        CompositionLocalProvider(
                            LocalMinimumInteractiveComponentEnforcement provides false
                        ) {
                            IconButton(
                                onClick = {
                                    today.value = today.value.minusDays(7)

                                },
                                modifier = Modifier
                                    .then(Modifier.size(20.dp))
                                    .statusBarsPadding()
                                    .background(
                                        color = Color.LightGray,
                                        shape = CircleShape
                                    ).tooltipAnchor(),
                                ) {
                                Icon(
                                    imageVector = (TablerIcons.CaretLeft),
                                    contentDescription = "Right",
                                    modifier = Modifier.size(10.dp)
                                )
                            }
                        }
                    }
                }

                Box (
                    //modifier = Modifier.padding(horizontal = 1.dp, vertical = 1.dp)
                ) {
                    PlainTooltipBox(
                        tooltip = {Text("Previous Week", color = Color.White)}
                    ) {
                        CompositionLocalProvider(
                            LocalMinimumInteractiveComponentEnforcement provides false
                        ) {
                            IconButton(
                                onClick = {
                                    today.value = today.value.plusDays(7)

                                },
                                modifier = Modifier
                                    .then(Modifier.size(20.dp))
                                    .statusBarsPadding()
                                    .background(
                                        color = Color.LightGray,
                                        shape = CircleShape
                                    ).tooltipAnchor(),

                                ) {
                                Icon(
                                    imageVector = (TablerIcons.CaretRight),
                                    contentDescription = "Right",
                                    modifier = Modifier.size(10.dp)
                                )
                            }
                        }
                    }
                }
            }

            // DAYS OF THE WEEK
            for (text in days) {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(1f)
                        .padding(0.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(text = text)
                        //style = TextStyle(color = Color.Black, fontSize = 12.sp)
                }
            }
        }

        // ACTUAL CALENDAR

        Row (
            modifier = Modifier
                .weight(0.84f)
                .fillMaxWidth()
                .verticalScroll(rememberScrollState())
                .drawBehind {
                    //print("DRAWBEHIND ENTERED \n")
                    //print("hourHeight in draw: " + hourHeight + "\n")
                    repeat(hours * 2) {
                        drawLine(
                            start = Offset(x = 0f, y = it * (hourHeight / 2).toPx().toFloat()),
                            end = Offset(x = size.width, y = it * (hourHeight / 2).toPx().toFloat()),
                            strokeWidth = 0.4.dp.toPx(),
                            color = Color.LightGray
                        )
                    }
                },

        ) {

            // make hourHeight adapt to changes in screenSize
            // make hourHeight adapt to changes in screenSize

            Column (
                modifier = Modifier
                    .width(70.dp)
            ) {
                //print("ScheduleSidebar run \n")
                //print("hourHeight into schedule" + hourHeight + "\n")
                ScheduleSidebar(hourHeight)
            }

            for (dayClass in classes) {
                Column(
                    modifier = Modifier
                        .weight(1f),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    val textSize = 10
                    Schedule(hourHeight = hourHeight, classes = dayClass, textSize)
                }
            }
        }
    }
}


