package components.playground

import APIclient.CourseSchedulesClient
import APIclient.CustomCalendarClient
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import components.calendar.AlternateSchedule
import components.calendar.Theme
import components.common.CustomIconButton
import components.courseInfo.padding
import components.courseSearch.DropSearch
import components.playground.optimization.OptimizationPage
import components.selectedCourses.ThemeDropdown
import components.store
import compose.icons.TablerIcons
import compose.icons.tablericons.*
import io.ktor.client.plugins.*
import kotlinx.coroutines.launch
import models.*
import java.time.LocalDateTime


@Immutable
sealed class AlternateScreen {
    object CalendarEdit : AlternateScreen()
    object Optimize : AlternateScreen()
}


@Composable
fun CalendarEditView(allCourses: List<CourseDetails>,
                     selectedCalendar: CustomCalendar,
                     toggleComputing: (Boolean) -> Unit,
                     goToCalendars: () -> Unit) {
    var isScheduleSheetOpen by remember { mutableStateOf(false) }
    var isListSheetOpen by remember { mutableStateOf(false) }
    var selectedCourse by remember { mutableStateOf("") }
    var courseTitle by remember { mutableStateOf("") }
    val courseNames = allCourses.map { "${it.subjectCode}${it.catalogNumber}" }
    val courseMap = allCourses.associateBy { it.subjectCode + it.catalogNumber }
    var schedules by remember {  mutableStateOf(emptyList<ScheduleData>()) }
    val listOfClasses = remember { mutableStateListOf<UserCalendarCourse>() }
    var currentScreen by remember { mutableStateOf<AlternateScreen>(AlternateScreen.CalendarEdit) }
    val calendarScope = rememberCoroutineScope()
    val selectedCourses = remember { mutableStateListOf<CourseSchedule>() }
    var isError by remember { mutableStateOf(false) }

    LaunchedEffect(true) {
        calendarScope.launch {
            try {
                val courses = CustomCalendarClient.getCalendarCourses(
                    store.getState().userId, selectedCalendar.id
                )
                listOfClasses.addAll(courses)

            } catch (e: Exception) {
                e.printStackTrace()
                isError = true
            }
        }
    }

    if (isError) {
        Row(modifier = Modifier.fillMaxSize().padding(vertical = 16.dp),
            horizontalArrangement = Arrangement.Center) {
            SelectionContainer {
                Text("The calendar no longer exists")
            }
        }
    } else {
        when (currentScreen) {
            is AlternateScreen.CalendarEdit -> {
                Draggable(modifier = Modifier.fillMaxSize()) {
                    Row (modifier = Modifier.fillMaxSize()) {
                        ScheduleTarget(isScheduleSheetOpen || isListSheetOpen, selectedCalendar, courseMap,
                            selectedCourse, listOfClasses, goToCalendars,
                            {
                                currentScreen = AlternateScreen.Optimize
                            }
                            ,
                            {
                                isListSheetOpen = !isListSheetOpen
                                isScheduleSheetOpen = false
                            })
                        { isScheduleSheetOpen = !isScheduleSheetOpen
                            isListSheetOpen = false
                        }
                        key(schedules) {
                            if (isScheduleSheetOpen) {
                                ScheduleSideSheet(courseNames, courseMap, schedules, courseTitle, { schedules = it },
                                { selectedCourse = it }) {
                                    courseTitle = it
                                }
                            }
                        }
                        if (isListSheetOpen) {
                            ListSideSheet(listOfClasses, selectedCalendar) {
                                listOfClasses.remove(it)
                            }
                        }
                    }
                }
            }

            AlternateScreen.Optimize -> {
                OptimizationPage(
                    selectedCalendar.id,
                    courseNames,
                    courseMap,
                    selectedCourses,
                    toggleComputing,
                    {
                        calendarScope.launch {
                            try {
                                val courses = CustomCalendarClient.getCalendarCourses(
                                    store.getState().userId, selectedCalendar.id
                                )
                                listOfClasses.removeAll(listOfClasses)
                                listOfClasses.addAll(courses)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                ) {
                    currentScreen = AlternateScreen.CalendarEdit
                }
            }
        }
    }
}

@Composable
fun ScheduleSideSheet(courseNames: List<String>,
                      courseMap:  Map<String, CourseDetails>,
                      schedules: List<ScheduleData>,
                      courseTitle: String,
                      setSchedules: (scheduleData: List<ScheduleData>) -> Unit,
                      setSelectedCourse: (courseName: String) -> Unit,
                      setCourseTitle: (courseTitle: String) -> Unit )
{

    val getScheduleScope = rememberCoroutineScope()
    Column (
        modifier = Modifier.fillMaxSize().drawBehind {
            val strokeWidth = 2f
            val y = size.height - strokeWidth

            drawLine(
                color = Color.Black,
                start = Offset(0f, 0f), //(0,0) at top-left point of the box
                end = Offset(0f, y),//bottom-left point of the box
                strokeWidth = strokeWidth
            )
        }
    ) {
        Box (modifier = Modifier.fillMaxSize()) {
            Box(
                modifier = Modifier.zIndex(1f)
            ) {
                Row(
                    modifier = Modifier.padding(horizontal = 8.dp, vertical = 10.dp).fillMaxWidth()
                        .height(IntrinsicSize.Min)
                ) {
                    DropSearch(courseNames, onClickCourse = {
                        getScheduleScope.launch {
                            try {
                                val course = courseMap[it]
                                if (course != null) {
                                    setSchedules( CourseSchedulesClient.getCourseSchedule(course.courseId)
                                        .sortedWith(compareBy(
                                            { it.courseComponent != "LEC" }, // First, order by whether termcode is not "LEC" (false first)
                                            { it.courseComponent != "TUT" }, // Second, order by whether termcode is not "TUT" (false first)
                                            { it.courseComponent != "TST" }, // Third, order by whether termcode is not "TST" (false first)
                                            { it.courseComponent }
                                        )).sortedBy { it.classSection })
                                    setCourseTitle(it + " " + course.title)
                                    setSelectedCourse(it)
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    })
                }

            }
            Box(modifier = Modifier.padding(top = 80.dp).padding(horizontal = 16.dp).fillMaxSize()) {
                Column (
                    modifier = Modifier.fillMaxSize()
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        SelectionContainer {
                            Text("Drag and drop a timeslot to the calendar to add it.", textAlign = TextAlign.Center)
                        }
                    }
                    if (courseTitle.isNotEmpty()) {
                        Row (
                            modifier = Modifier.fillMaxWidth().padding(horizontal = 10.dp).border(0.dp, Color.Black)
                                .background(Color.Gray).padding(horizontal = 6.dp, vertical = 10.dp),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            SelectionContainer {
                                Text(
                                    text = courseTitle, fontWeight = FontWeight.Bold,
                                    modifier = Modifier.padding(horizontal = 8.dp, vertical = 10.dp),
                                    textAlign = TextAlign.Center
                                )
                            }
                        }
                    }
                    LazyColumn(
                        modifier = Modifier.fillMaxSize(),
                        contentPadding = PaddingValues(horizontal = 10.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        items(schedules) {
                            DraggableSchedule(it) }
                    }
                }
            }
        }
    }
}

@Composable
fun ListSideSheet(
                  courseList: SnapshotStateList<UserCalendarCourse>,
                  selectedCalendar: CustomCalendar,
                  removeCourse: (c : UserCalendarCourse) -> Unit) {
    val courseListMap = courseList.groupBy { "${it.courseNum} - ${it.courseTitle}" }
    val coursesScope = rememberCoroutineScope()
    val snackbarState = remember { SnackbarHostState() }
    Scaffold(
        containerColor = Color.Transparent,
        snackbarHost = {
            SnackbarHost(hostState = snackbarState) {
                Snackbar(
                    snackbarData = it,
                    modifier = Modifier.width(200.dp),

                    )
            }
        },
    ) {
        Column (
            modifier = Modifier.fillMaxSize().drawBehind {
                val strokeWidth = 2f
                val y = size.height - strokeWidth

                drawLine(
                    color = Color.Black,
                    start = Offset(0f, 0f), //(0,0) at top-left point of the box
                    end = Offset(0f, y),//bottom-left point of the box
                    strokeWidth = strokeWidth
                )
            }
        ) {
            Row(
                Modifier.fillMaxWidth().padding(vertical = 20.dp, horizontal = 8.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                SelectionContainer {
                    Text(
                        text = "List of Selected Courses",
                        fontSize = 20.sp
                    )
                }
                Row () {
                    CustomIconButton(
                        onClick= {
                            coursesScope.launch {
                                try {
                                    val response = CustomCalendarClient
                                        .updateCalendar(store.getState().userId, selectedCalendar.id
                                            , courseList.toList())
                                    if (response) {
                                        snackbarState.showSnackbar(
                                            message = "Save Successful",
                                            duration = SnackbarDuration.Short
                                        )
                                    }
                                }catch (e: ClientRequestException) {
                                    println("Error fetching data: ${e.message}")
                                }
                            }
                        },
                        modifier= Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                        tooltipText= "Save Calendar",
                        buttonRadius= 36.dp,
                        buttonSize= 15.dp,
                        backgroundColor = Color.LightGray,
                        icon = TablerIcons.DeviceFloppy
                    )
                }
            }
            Row(
                modifier = Modifier.fillMaxWidth()
                    .padding(vertical = 20.dp, horizontal = 8.dp),)
            {
                LazyColumn(Modifier.padding(0.dp)) {
                    items(courseListMap.keys.toList()) {
                        courseListMap[it]?.let { it1 -> CalendarCourseCluster(it1, it, removeCourse) }
                        Spacer(modifier = Modifier.height(10.dp))
                    }
                }
            }
        }
    }
}

@Composable
fun CalendarCourseCluster(components: List<UserCalendarCourse>, name: String,
                          removeCourse: (c: UserCalendarCourse) -> Unit) {
    Column (
        Modifier.fillMaxWidth()
    ) {
        Row (
            modifier = Modifier.fillMaxWidth().border(0.dp, Color.Black)
                .background(Color.LightGray).padding(horizontal = 16.dp),
        ) {
            SelectionContainer {
                Text(text = name, fontWeight = FontWeight.Bold, modifier = Modifier.padding(vertical = 4.dp))
            }
        }
        Row (modifier = Modifier.fillMaxWidth().border(0.dp, Color.Black)
            .padding(horizontal = 12.dp, vertical = 4.dp)) {
            Column {
                for (course in components) {
                    Row (
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        SelectionContainer {
                            Text(text = course.component, modifier = Modifier.padding(vertical = 5.dp))
                        }
                        CustomIconButton(
                            onClick= {removeCourse(course)},
                            modifier= Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                            tooltipText= "Remove Course",
                            buttonRadius= 36.dp,
                            buttonSize= 15.dp,
                            backgroundColor = Color.LightGray,
                            icon = TablerIcons.Trash
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun DraggableSchedule(courseSection: ScheduleData) {
    DragTarget(
        modifier = Modifier,
        dataToDrop = courseSection
    ) {
        Column(
            modifier = Modifier.border(0.dp, Color.Black).widthIn(max = 300.dp).height(80.dp)
        ) {
            Row(
                Modifier.background(Color.LightGray).fillMaxHeight(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                val pad = padding(courseSection.classSection)
                ScheduleCell(text = courseSection.courseComponent + " " + pad + courseSection.classSection, weight = 0.2f)
                ScheduleCell(
                    text = "${
                        LocalDateTime.parse(courseSection.scheduleData?.get(0)?.classMeetingStartTime.orEmpty())
                            .format(components.calendar.TimeFormatter).replace(".", "").uppercase()
                    } - " +
                            LocalDateTime.parse(courseSection.scheduleData?.get(0)?.classMeetingEndTime.orEmpty())
                                .format(components.calendar.TimeFormatter).replace(".", "").uppercase(), weight = 0.5f
                )
                ScheduleCell(
                    text = courseSection.scheduleData?.get(0)?.classMeetingDayPatternCode.orEmpty(),
                    weight = 0.2f
                )
            }
        }
    }
}

@Composable
fun RowScope.ScheduleCell(
    text: String,
    weight: Float,
) {
    Text(
        text = text,
        modifier = Modifier
            .weight(weight, fill = true)
            .padding(6.dp),
        textAlign = TextAlign.Center,
        fontSize = 10.sp
    )
}

@Composable
fun ScheduleTarget(isSheetOpen: Boolean,
                   selectedCalendar: CustomCalendar, courseMap: Map<String, CourseDetails>,
                   selectedCourse: String,
                   listOfClasses: SnapshotStateList<UserCalendarCourse>,
                   goToCalendars: () -> Unit,
                   goToOptimize: () -> Unit,
                   toggleListSideSheet: () -> Unit,
                   toggleScheduleSideSheet: () -> Unit) {

    val addCourseScope = rememberCoroutineScope()

    var expanded by remember { mutableStateOf(false) }
    var selectedTheme by remember { mutableStateOf(Theme.OCEAN) }

    Column (
        modifier = Modifier.fillMaxHeight().fillMaxWidth(if (isSheetOpen) { 0.7f} else {1f})
    ) {
        Row(
            Modifier.fillMaxWidth().padding(top = 15.dp).padding(horizontal = 12.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row (
                verticalAlignment = Alignment.CenterVertically
            ) {
                CustomIconButton(
                    onClick= goToCalendars,
                    modifier= Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                    tooltipText= "Go Back",
                    buttonRadius= 36.dp,
                    buttonSize= 15.dp,
                    backgroundColor = Color.LightGray,
                    icon = TablerIcons.ChevronLeft
                )
                SelectionContainer {
                    Text(
                        text = selectedCalendar.name,
                        color = Color.Black,
                        fontSize = 25.sp,
                        maxLines = 1
                    )
                }
            }
            Row () {

                fun expandedFunc(tf: Boolean) {
                    expanded = tf
                }

                fun selectedThemeFunc(theme: Theme) {
                    selectedTheme = theme
                }

                ThemeDropdown(expanded, selectedTheme, ::expandedFunc, ::selectedThemeFunc)

                CustomIconButton(
                    onClick= toggleListSideSheet,
                    modifier= Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                    tooltipText= "List View",
                    buttonRadius= 36.dp,
                    buttonSize= 15.dp,
                    backgroundColor = Color.LightGray,
                    icon = TablerIcons.List
                )
                CustomIconButton(
                    onClick= toggleScheduleSideSheet,
                    modifier= Modifier.padding(horizontal = 10.dp, vertical = 5.dp),
                    tooltipText= "Add Course",
                    buttonRadius= 36.dp,
                    buttonSize= 15.dp,
                    backgroundColor = Color.LightGray,
                    icon = TablerIcons.Plus
                )
                Button(
                    onClick = goToOptimize,
                    modifier = Modifier.width(180.dp),
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text("Optimize", color = Color.White)
                    }
                }
            }
        }
        DropTarget<ScheduleData>(
            modifier = Modifier
        ) {
                isInBound, schedule ->
            schedule?.let {
                if (isInBound) {
                    addCourseScope.launch {
                        try {
                            val toAdd = courseMap[selectedCourse]
                            if (toAdd != null) {
                                val pad = padding(schedule.classSection)
                                val response = CustomCalendarClient.addCalendarCourse(store.getState().userId,
                                    selectedCalendar.id, UserCalendarCourse(toAdd.courseId,
                                        toAdd.subjectCode + " " + toAdd.catalogNumber,
                                        toAdd.title, schedule.courseComponent + " " + pad + schedule.classSection,
                                        schedule.scheduleData?.get(0)?.classMeetingStartTime.orEmpty(),
                                        schedule.scheduleData?.get(0)?.classMeetingEndTime.orEmpty(),
                                        schedule.scheduleData?.get(0)?.classMeetingDayPatternCode.orEmpty())
                                )
                                if (response != null) {
                                    listOfClasses.add(response)
                                } else {
                                    // error do nothing
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    LocalDragTargetInfo.current.dataToDrop = null
                }
            }

            AlternateSchedule(listOfClasses, selectedTheme)
        }
    }
}