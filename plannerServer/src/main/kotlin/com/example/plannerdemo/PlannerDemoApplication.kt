package com.example.plannerdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PlannerDemoApplication

fun main(args: Array<String>) {
    runApplication<PlannerDemoApplication>(*args)
}
