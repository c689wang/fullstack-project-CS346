# Release Notes for Sprint 3

**Version:** 0.3  
**Release Date:** Nov 17, 2023

## Summary
Sprint 3 has been a significant step forward in enhancing the functionality and user experience of our application. Key features introduced in this sprint include the ability to create and view alternative schedules, a drag-and-drop feature for schedule management, comprehensive reworking of the UI and logic for major pages, and the introduction of an explore page for course browsing. We have also implemented a logout button, added date functionality to the calendar, and redesigned the side bar menu for better navigation.

## New Features

### Alternative Schedules
- Users can now create and view alternative schedules, offering flexibility in managing different class or lecture/tutorial times.
- Alternate schedules and their respective courses are also persisted in the database and custom API routes are defined for persistence actions.

### Drag and Drop for Alternate Schedules
- Enhanced interactivity with the ability to drag and drop course components from schedules directly into alternate calendars.

### Reworked UI and Logic
- **Course Info Page:** Adjusted table row heights and widths, sorted schedule information by component (LEC, TUT, TST).
- **Search Bar in Course Info Page:** Integrated search functionality directly on the course info page.
- **Course Selection Page:** Improved UI to group sections by class and display them in table format, with a delete icon button.
- **Side Sheet in Course Selection:** Facilitated direct addition of courses to the user's current schedule.
- **Home Page:** Customised the home page with a welcome message and the current date.

### Explore Page
- Users can now explore all courses for a specific subject and navigate directly to detailed course information.

### Filter Course Results
- Implemented a feature allowing users to filter courses on the explore page by catalogue number categories.

### Logout Button
- Added a logout button to the sidebar for easy user sign-out.

### Calendar Enhancements
- The calendar now shows the current week and allows users to switch between weeks.
- Test course components are only shown on the calendar on their respective dates.

### Redesigned Side Bar Menu
- Introduced a new layout for the side bar menu, including routes to main features like Home, Course Search, Friends, Wishlist, and Calendars.

### Conflict Detection
- Implemented mechanic to warn users when a course conflict arise when adding courses to users' current calendars.

## Notes for Users
We recommend exploring the new alternative schedules feature for optimal course planning. Please familiarize yourself with the reworked UI elements for a more seamless user experience.

## Upcoming in Future Sprints
Stay tuned for further enhancements to the authentication system, additional user interface improvements, and more robust course planning tools.
